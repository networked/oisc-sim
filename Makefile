CFLAGS=-g -O2 -Wall
INCS = 
LIBS = 

all: mov-rev
	
mov-rev: mov-rev.o
	$(CC) $^ -o $@ $(CFLAGS) $(INCS) $(LIBS)

clean:
	rm -f *.o mov-rev

test: mov-rev
	./mov-rev < test.oisc

fact: mov-rev
	cpp fact.oisc > fact.res
	./mov-rev -f fact.res
	rm fact.res

