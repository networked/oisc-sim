/* This file is distributed under the GNU GPL license.
 * (C) 25/03/2006, 2013 by networked */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>
#include <unistd.h>
#include <getopt.h>

#define ADDR unsigned int
#define REG_TYPE unsigned int
#define PROG_LENGTH 4096
#define REGS_N 16
#define STR_MAX_LENGTH 255
#define DEBUG 0

#define REG_PC 3
#define REG_IO 4
#define REG_ADD 5
#define REG_SUB 6
#define REG_MUL 7
#define REG_DIVNZ 8
#define REG_GT 9
#define REG_EQ 10
#define REG_CONST 11
#define REG_CHOOSE 12

struct command
{
    ADDR in;
    ADDR out;
    char* comment;
};

void regdump(REG_TYPE* regs)
{
    int i;
    for (i = 0; i < REGS_N; i++) {
        printf("R%2u: %3u; ", i, regs[i]);
    }
    printf("\n");
}

void printcommand(struct command c)
{
    printf("%u -> %u (%s)\n", c.in, c.out, c.comment);
}

void usage(char* me)
{
    printf("Usage: %s [-f filename | -h]\n", me);
}

void help()
{
    printf(
"    This is an OISC simulator with the following command syntax:\n"
"        a b\n"
"    where a and b are in range 0..%u; this sends the value of memory\n"
"    cell B to memory cell A. That's it. All computations are performed\n"
"    using a memory mapped ALU:\n"
"        c[%u] = c[0] + c[1]\n"
"        c[%u] = c[0] - c[1]\n"
"        c[%u] = c[0] * c[1]\n"
"        c[%u] = c[0] / c[1]\n"
"        c[%u] = c[0] > c[1]\n"
"        c[%u] = c[0] == c[1]\n"
"        c[%u] = c[0] ? c[1] : c[2]\n"
"    Also, c[%u] is the instruction pointer, c[%u] is for numerical I/O\n"
"    and c[%u] is for literal input.\n\n"
"    - networked, (C) 25/03/2006, 2013.\n",
    REGS_N - 1,
    REG_ADD, REG_SUB, REG_MUL, REG_DIVNZ, REG_GT, REG_EQ, REG_CHOOSE,
    REG_PC, REG_IO, REG_CONST);
}

int main(int argc, char* argv[])
{
    char* infilename = NULL;
    FILE* infile = stdin;

    int ch;
    while ((ch = getopt(argc, argv, "f:h")) != -1) {
        switch (ch) {
            case 'f':
                infilename = optarg;
                break;
            case 'h':
                usage(argv[0]);            
                help();
                return EX_OK;
                break;
            default:
                usage(argv[0]);
                return EX_NOINPUT;                
        }
    }

    if (infilename != NULL) {
        if (access(infilename, R_OK) == 0) {
            infile = fopen(infilename, "r");
        } else {
            printf("error: can't open file `%s'\n\n", infilename);
            usage(argv[0]);            
            return EX_NOINPUT;
        }
    }

    struct command code[PROG_LENGTH];
    REG_TYPE regs[REGS_N];
    int i = 0;
    int n;
    int debug = 0 || DEBUG;
    char s[STR_MAX_LENGTH];
    for(;;) {
        char* res1 = fgets(s, STR_MAX_LENGTH, infile);
        if (s[strlen(s) - 1] == '\n') {
            s[strlen(s) - 1] = '\0';
        }
        if (res1 == NULL || feof(infile)) {
            break;
        }
        if (strcmp(s, "") == 0) {
            continue;
        }
        
        if (debug) {
            code[i].comment = (char*) calloc(100, sizeof(char));
            strcpy(code[i].comment, s);
        }

        if (sscanf(s, "%u %u", &code[i].in, &code[i].out) == 2) {
            i++;
        }
        printf("%s %d\n", s, i);
    }
    if (infile != stdin) {
        fclose(infile);
    }

    n = i;

    printf("Executing program (%d lines):\n", n - 1);
    for (i = 0; i < REGS_N; i++) {
        regs[i] = 0;
    }
    for (i = 0; i < n; i++) {
        regs[code[i].out] = code[i].out != REG_CONST ? regs[code[i].in] :
                            code[i].in;
        regs[REG_ADD] = regs[0] + regs[1];
        regs[REG_SUB] = regs[0] - regs[1];
        regs[REG_MUL] = regs[0] * regs[1];
        regs[REG_DIVNZ] = regs[1] != 0 ? regs[0] / regs[1] : 0;
        regs[REG_GT] = regs[0] > regs[1];
        regs[REG_EQ] = regs[0] == regs[1];
        regs[REG_CHOOSE] = regs[0] ? regs[1] : regs[2];
        if (code[i].in == REG_IO && code[i].out != REG_CONST) {
            int res = 0;
            char s[STR_MAX_LENGTH];
            do {
                if (fgets(s, STR_MAX_LENGTH, stdin)) {
                    res = sscanf(s, "%u", &(regs[code[i].out]));
                }
            } while (res != 1);
        };
        if (code[i].out == REG_IO) {
            printf("--- %u\n", regs[code[i].in]);
        };
        if (code[i].out == REG_PC) {
            i = regs[REG_PC] - 2;
        } else {
            regs[REG_PC] = i;
        };
        if (debug) {
            printcommand(code[i]);
            regdump(regs);
        }
    }
    printf("\nFinal result:\n");
    regdump(regs);
    return 0;
}
